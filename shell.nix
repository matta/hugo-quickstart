{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  # nativeBuildInputs is usually what you want -- tools you need to run
  nativeBuildInputs = [
    # The cacert package is needed by git because this project's CMake config fetches
    # packages from github using https.  See
    # https://github.com/NixOS/nixpkgs/issues/64212#issuecomment-1244404378
    #pkgs.cacert

    #pkgs.git

    pkgs.go_1_18

    # Various utilities
    pkgs.entr
    pkgs.hugo

    # This is https://github.com/svenkreiss/html5validator
    # which uses https://github.com/validator/validator.
    pkgs.html5validator
  ];
}
